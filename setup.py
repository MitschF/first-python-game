import cx_Freeze

executables = [cx_Freeze.Executable("main.py", base="Win32GUI")]

cx_Freeze.setup(
    name="Crossy RPG",
    options={"build_exe": {"packages": ["sys", "pygame", "random"], "include_files": [
        "assets/"]}},
    executables=executables
)
