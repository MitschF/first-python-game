# Pygame Setup
import sys
import pygame
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
WHITE_COLOR = (255, 255, 255)
BLACK_COLOR = (0, 0, 0)
TITLE = 'Crossy RPG'
clock = pygame.time.Clock()
pygame.font.init()
font = pygame.font.SysFont('Adobe New Century Schoolbook', 75)

# Global Variable to kill game on user Input
is_game_over = False


class Game:
    TICK_RATE = 60

    def __init__(self, image_path, width, height, title, bg_color):
        self.title = title
        self.width = width
        self.height = height
        self.bg_color = bg_color

        background_image = pygame.image.load(image_path)
        self.image = pygame.transform.scale(background_image, (width, height))

        # Set up game-screen with width and height
        self.game_screen = pygame.display.set_mode((width, height))
        # Fill background of the game with a color
        self.game_screen.fill(bg_color)
        # Set the title of the game
        pygame.display.set_caption(title)

    def runGameLoop(self, level):
        self.level = level
        global is_game_over
        direction = 0

        player = PlayerCharacter(
            'assets/player.png', 'Mitsch', 100, self.width/2-25, self.height-70, 50, 50, 8)
        treasure = GameObject('assets/treasure.png',
                              'Treasure', 200, self.width/2-25, 20, 50, 50, 0)
        monsters = [NPC('assets/enemy.png',
                        'Enemy',
                        20,
                        random.randint(20, self.width-70),
                        random.randint(180, self.height-180),
                        50,
                        50,
                        random.randint(8, 12)) for i in range(self.level)]

        while not is_game_over:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    is_game_over = True
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        is_game_over = True
                    if event.key == pygame.K_UP or event.key == pygame.K_w:
                        direction = 1
                    elif event.key == pygame.K_DOWN or event.key == pygame.K_s:
                        direction = -1
                elif event.type == pygame.KEYUP:
                    if event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_w or event.key == pygame.K_s:
                        direction = 0

            self.game_screen.fill(self.bg_color)
            self.game_screen.blit(self.image, (0, 0))

            for monster in monsters:
                monster.move(self)
                monster.draw(self.game_screen)

            treasure.draw(self.game_screen)

            player.move(self, direction)
            player.draw(self.game_screen)

            for monster in monsters:
                if player.detect_collision(monster):
                    text = font.render('You lose!', True, BLACK_COLOR)
                    self.game_screen.blit(
                        text, (self.width / 2 - 100, self.height / 2))
                    pygame.display.update()
                    clock.tick(1)
                    self.runGameLoop(1)

            if player.detect_collision(treasure):
                text = font.render(
                    'Level: ' + str(self.level + 1) + "!", True, BLACK_COLOR)
                self.game_screen.blit(
                    text, (self.width // 2 - 100, self.height / 2))
                pygame.display.update()
                clock.tick(1)
                self.runGameLoop(level + 1)

            pygame.display.update()
            clock.tick(self.TICK_RATE)

        return


class GameObject:
    def __init__(self, img_path, name, health, x_pos, y_pos, width, height, speed):
        object_image = pygame.image.load(img_path)
        self.image = pygame.transform.scale(object_image, (width, height))

        self.name = name
        self.health = health
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.speed = speed

        self.width = width
        self.height = height

    def draw(self, background):
        background.blit(self.image, (self.x_pos, self.y_pos))


class PlayerCharacter(GameObject):

    def __init__(self, img_path, name, health, x_pos, y_pos, width, height, speed):
        super().__init__(img_path, name, health, x_pos, y_pos, width, height, speed)

    def move(self, game_screen, direction):
        if direction > 0 and (self.y_pos - self.speed) >= 35:
            self.y_pos -= self.speed
        elif direction < 0 and (self.y_pos + self.speed) <= game_screen.height - self.height - 20:
            self.y_pos += self.speed

    def detect_collision(self, target):
        if self.y_pos > target.y_pos + target.height-8:
            return False
        elif self.y_pos + self.height < target.y_pos+8:
            return False

        if self.x_pos > target.x_pos + target.width-8:
            return False
        elif self.x_pos + self.width < target.x_pos+8:
            return False

        return True


class NPC(GameObject):
    def __init__(self, img_path, name, health, x_pos, y_pos, width, height, speed):
        super().__init__(img_path, name, health, x_pos, y_pos, width, height, speed)

    def move(self, game_screen):
        if self.x_pos <= 20:
            self.speed = abs(self.speed)
        elif self.x_pos >= game_screen.width - self.width - 20:
            self.speed = -abs(self.speed)

        self.x_pos += self.speed


pygame.mixer.pre_init(44100, 16, 2, 4096)
pygame.mixer.init()
pygame.mixer.music.load("assets/Medieval_Theme_1.wav")
pygame.mixer.music.play(-1)

pygame.init()

new_game = Game('assets/background.png', 1000, 1000, TITLE, WHITE_COLOR)
new_game.runGameLoop(1)

pygame.quit()
sys.exit()
